var http = require("http");

function onRequest(request, response) {
  console.log("Request received.");
  response.writeHead(200, {"Content-Type": "text/plain"});
  response.write("Welcome to node.js World");
  response.end();
}

http.createServer(onRequest).listen(9000);
console.log("Server has started.");