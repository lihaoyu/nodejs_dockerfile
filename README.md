Clone Dockerfile

    git clone https://lihaoyu@bitbucket.org/lihaoyu/nodejs_dockerfile.git


Build Image

    cd nodejs_dockerfile
    docker build -t nodejs:10.13.0 .


Build Container

    docker run -d -p 31122:22 -p 38080:8080 --name nodejs nodejs:10.13.0 /root/start.sh
	
	
RUN Test
	
	node index.js