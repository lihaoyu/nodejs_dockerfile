############################################################
# Dockerfile build Go
# in ubuntu
############################################################
# Base image to ubuntu:18.04
FROM ubuntu:18.04

# Install Ubuntu
RUN \
    apt-get update && \
    apt-get -y upgrade && \
    apt-get install -y build-essential && \
    apt-get install -y software-properties-common && \
    apt-get install -y curl git vim autossh rsync screen && \
    rm -rf /var/lib/apt/lists/*

RUN apt-get update

# Install SSHD
RUN apt-get install -y openssh-server openssh-client && \
    mkdir /var/run/sshd

# Get newest version of nodejs

RUN cd ~ && \
    curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh && \
    bash nodesource_setup.sh

# Install Nodejs

RUN apt-get update && \
    apt-get install -y nodejs

# Define working directory.
WORKDIR /root

#Add file from dir
COPY ./start.sh /root
COPY ./sshd_config /etc/ssh/sshd_config
COPY ./index.js /root

#Change Access
RUN chmod u+x /root/start.sh

#CMD node index.js

#Open Port
EXPOSE 8080
EXPOSE 22

# Define default command.
CMD ["/bin/bash", "/root/start.sh"]
